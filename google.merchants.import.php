<?

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (CModule::IncludeModule('iblock')) {

    $categoryClass = \Local\Highload::get('GoogleCategory');

    pre($categoryClass);

//    pre($arEnumValue);
    foreach (file(__DIR__ . '/taxonomy-with-ids.ru-RU.txt') as $key => $line) {
        if (empty($key)) {
            continue;
        }
        list($code, $value) = array_map('trim', explode('-', $line));


        $rsData = $categoryClass::getList(array(
                    "select" => array('ID'),
                    "filter" => array('UF_CODE' => $code)
        ));
        $rsData = new \CDBResult($rsData);
        if (!$arItem = $rsData->Fetch()) {
            $arBxData = array(
                'UF_CODE' => $code,
                'UF_NAME' => str_replace('>', ':', $value),
                'UF_GOOGLE' => $value,
            );
            $result = $categoryClass::add($arBxData);
            if (!$result->isSuccess()) {
            }
        } else {
            $arBxData = array(
                'UF_GOOGLE' => $value,
            );
            $categoryClass::update($arItem['ID'], $arBxData);
        }
//    exit;
    }
}