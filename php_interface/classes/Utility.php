<?php

namespace Local;

class Utility {

    const IS_CACHE = true;
    const CACHE_TIME = 3600;
    const CACHE_DIR = '/local/';

    static public function useCache($cacheId, $func, $time = self::CACHE_TIME) {
        $obCache = new \CPHPCache;
        $cacheId = 'local:' . $time . ':' . (is_array($cacheId) ? implode(':', $cacheId) : $cacheId);
        if (self::IS_CACHE and $obCache->InitCache($time, $cacheId, self::CACHE_DIR)) {
            $arResult = $obCache->GetVars();
        } elseif ($obCache->StartDataCache()) {
            $arResult = $func();
            $obCache->EndDataCache($arResult);
        }
        return $arResult;
    }

    static public function translate($str) {
        static $t = array('а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'jo', 'ж' => 'gh', 'з' => 'z', 'и' => 'i',
            'й' => 'j', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f',
            'х' => 'x', 'ц' => 'c', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'th', 'ъ' => '', 'ь' => '', 'ы' => 'y', 'э' => 'eh', 'ю' => 'ju', 'я' => 'ja');

        $new = '';
        $str = strtolower(trim($str));
        $str = preg_replace('~\s~S', '_', $str);
        for ($i = 0, $c = strlen($str); $i < $c; $i++) {
            $s = substr($str, $i, 1);
            if (isset($t[$s])) {
                $new .= $t[$s];
            } else {
                $new .= $s;
            }
        }
        return $new;
    }

}
