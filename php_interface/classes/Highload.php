<?php

namespace Local;

use Bitrix\Main\Loader;

class Highload {

    static public function get($name) {
        static $cache = array();
        if (empty($cache[$name])
                and Loader::includeModule('iblock')
                and Loader::includeModule('highloadblock')) {

            $arData = \Bitrix\Highloadblock\HighloadBlockTable::getList(array('filter' => array('NAME' => $name)))->fetch();
            $cache[$name] = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($arData)->getDataClass();
        }
        return $cache[$name];
    }

}
