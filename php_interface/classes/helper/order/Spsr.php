<?php

namespace Local\Helper\Order;

use Local\Highload,
    CModule,
    CIBlockElement;

class Spsr {

    const TABLE_NAME = 'OrderSpsr';

    static public function add($orderId) {
        $orderSpsrClass = Highload::get(self::TABLE_NAME);
        $rsData = $orderSpsrClass::getList(array(
                    "select" => array('UF_STATUS'),
                    "filter" => array('UF_ORDER' => $orderId)
        ));
        $rsData = new \CDBResult($rsData);
        if (!$arItem = $rsData->Fetch()) {
            $arBxData = array(
                'UF_ORDER' => $orderId,
                'UF_STATUS' => 0
            );
            $result = $orderSpsrClass::add($arBxData);
            if (!$result->isSuccess()) {
                throw new \Exception('Ошибка отправки заказа в spsr');
            }
        }
//        preExit();
    }

    function agent() {
        try {
            $orderSpsrClass = Highload::get(self::TABLE_NAME);
            $rsData = $orderSpsrClass::getList(array(
                        "select" => array('ID', 'UF_ORDER'),
                        "filter" => array(
                            'UF_STATUS' => 0,
                        )
            ));
            $rsData = new \CDBResult($rsData);
            while ($arItem = $rsData->Fetch()) {
                if (self::SaleOrder($arItem['UF_ORDER'])) {
                    $result = $orderSpsrClass::update($arItem['ID'], array('UF_STATUS' => 1));
                    if (!$result->isSuccess()) {
                        throw new \Exception($result->getErrorMessages());
                    }
                }
            }
        } catch (Exception $ex) {
            
        }
        return '\Local\Helper\Order\Spsr::agent();';
    }

    function SaleOrder($orderId) {
        //global $USER;
        //if( !$USER ) $USER = new CUser;
        //var_dump( $USER->GetID() );
        //echo "<pre>";
        //var_dump($USER);
        //echo "</pre>";
        //mail("delllevit@gmail.com", "Вынрузка окончена", "Line 1\nLine 2\nLine 3");
        //die;
        //if( !$USER ){
        //    $USER = new CUser;
        //}
        //echo "<pre>";
        //var_dump($USER->GetID());
        //echo "</pre>";
        //die;
        //$user_test =


        $snipStatusDec = array(
            "PZ" => 1,
            "PD" => 2,
            "ST" => 3,
            "OZ" => 4,
            "DF" => 3,
            "DN" => 3,
        );

        $snipNameDec = array(
            "Курьерская доставка" => "SP",
            "Почта России" => "RP"
        );

        CModule::IncludeModule("sale");
        $order = \Bitrix\Sale\Order::load($orderId); //заказ;

        $propertyCollection = $order->getPropertyCollection(); //свойства пользователя

        $SPSR["PROP"] = array(
            "ID" => $order->getId(),
            "DATA" => $order->getField("DATE_STATUS")->format("Y-m-d"),
            "PRICE" => $order->getPrice(),
            "DELIVERY_PRICE" => $order->getDeliveryPrice(),
            "IS_PAID" => ( $order->isPaid() ) ? "true" : "false",
            "USER_ID" => $order->getUserId(),
            "USER_FIO" => $propertyCollection->getItemByOrderPropertyId(1)->getValue(),
            "USER_PROP" => array(
                "FullAddress" => $propertyCollection->getItemByOrderPropertyId(25)->getValue(),
                "Postcode" => $propertyCollection->getItemByOrderPropertyId(4)->getValue(),
                "State" => $propertyCollection->getItemByOrderPropertyId(27)->getValue(),
                "City" => $propertyCollection->getItemByOrderPropertyId(26)->getValue(),
                "Street" => $propertyCollection->getItemByOrderPropertyId(20)->getValue(),
                "Building" => $propertyCollection->getItemByOrderPropertyId(21)->getValue(),
                "Building2" => $propertyCollection->getItemByOrderPropertyId(22)->getValue(),
                "Room" => $propertyCollection->getItemByOrderPropertyId(23)->getValue(),
                "Phone" => str_replace(array("+", " ", "(", ")", "-"), "", $propertyCollection->getItemByOrderPropertyId(3)->getValue()),
            ),
        );



        $snipItems = $order->getShipmentCollection();
        foreach ($snipItems as $snipItem) {
            if (!$snipItem->isSystem()) {

                $SPSR["PROP"]["DELIVERY_CODE"] = $snipNameDec[$snipItem->getField("DELIVERY_NAME")];
                $SPSR["PROP"]["DELIVERY_TYPE"] = $snipStatusDec[$snipItem->getField("STATUS_ID")];
            }
        }

        $basket = $order->getBasket();
        CModule::IncludeModule("iblock");
        foreach ($basket->getBasketItems() as $item) {

            $artnumber = CIBlockElement::GetProperty(CIBlockElement::GetIBlockByID($item->getProductId()), $item->getProductId(), array("sort" => "asc"), Array("CODE" => "ARTIKUL"))->Fetch()["VALUE"];
            if ($artnumber) {

                $SPSR["PROP"]["BUSKET_ITEMS"][] = array(
                    "ID" => $artnumber,
                    "PRICE" => $item->getPrice(),
                    "QUANTITY" => $item->getQuantity(),
                    "FULL_PRICE" => round($item->getPrice() * $item->getQuantity(), 2),
                );
            }
        }


        //echo "<pre>";
        //var_dump( $SPSR["PROP"]["BUSKET_ITEMS"] );
        //echo "</pre>";
        //die;
        // isAllowDelivery();  true, если разрешена доставка
        //isShipped(); true, если отправлен

        //когда в битрикс будет передаваться ID_1C можно включить фильтр, чтобы на 100% значть что заказ уже в 1с
        if ($order->getField('ID_1C') and $order->isAllowDelivery() && $order->isShipped()) {

            $user = '<Command id="PutContractors" Login="ModaOnline" Password="kg6KjhU3s">
                    <Contractors>
                        <Contractor>
                            <Id>' . $SPSR["PROP"]["USER_ID"] . '</Id>
                            <Name>' . $SPSR["PROP"]["USER_FIO"] . '</Name>
                            <Fullname>' . $SPSR["PROP"]["USER_FIO"] . '</Fullname>
                        </Contractor>
                    </Contractors>
                </Command>';

            $url = "https://3pl.spsr.ru/webapi/execute";

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            curl_setopt($ch, CURLOPT_TIMEOUT, 500);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $user);

            $req = curl_exec($ch);
            curl_close($ch);

            $out = '<Command Id="SalesOrder" Login="ModaOnline" Password="kg6KjhU3s">
            <Orders>
                <Order>
                    <Document>
                        <ID>' . $SPSR["PROP"]["ID"] . '</ID>
                        <Date>' . $SPSR["PROP"]["DATA"] . '</Date>
                        <TotalSum>' . $SPSR["PROP"]["PRICE"] . '</TotalSum>
                        <SumOfDelivery>' . $SPSR["PROP"]["DELIVERY_PRICE"] . '</SumOfDelivery>
                        <DeliveryCompany>' . $SPSR["PROP"]["DELIVERY_CODE"] . '</DeliveryCompany>
                        <IsPayed>' . $SPSR["PROP"]["IS_PAID"] . '</IsPayed>
                        <ProcessType>' . $SPSR["PROP"]["DELIVERY_TYPE"] . '</ProcessType>
                        <Comment></Comment>
                        <DeliveryInformation>
                            <FullAddress>' . $SPSR["PROP"]["USER_PROP"]["FullAddress"] . '</FullAddress>
                            <Postcode>' . $SPSR["PROP"]["USER_PROP"]["Postcode"] . '</Postcode>
                            <State>' . $SPSR["PROP"]["USER_PROP"]["State"] . '</State>
                            <City>' . $SPSR["PROP"]["USER_PROP"]["City"] . '</City>
                            <Street>' . $SPSR["PROP"]["USER_PROP"]["Street"] . '</Street>
                            <Building>' . $SPSR["PROP"]["USER_PROP"]["Building"] . '</Building>
                            <Building2>' . $SPSR["PROP"]["USER_PROP"]["Building2"] . '</Building2>
                            <Room>' . $SPSR["PROP"]["USER_PROP"]["Room"] . '</Room>
                            <Phone>' . $SPSR["PROP"]["USER_PROP"]["Phone"] . '</Phone>
                        </DeliveryInformation>
                        <ConsumerID>' . $SPSR["PROP"]["USER_ID"] . '</ConsumerID>
                    </Document>
                    <Items>';

            foreach ($SPSR["PROP"]["BUSKET_ITEMS"] as $item) {

                $nds_sum = round($item["PRICE"] * 18 / 118, 2);
                $total_nds = $nds_sum * $item["QUANTITY"];
                $no_vat = round($item["PRICE"] * $item["QUANTITY"] - $total_nds, 2);
                $out .= '
                            <Item>
                                <ID>' . $item["ID"] . '</ID>
                                <Quantity>' . $item["QUANTITY"] . '</Quantity>
                                <Price>' . $item["PRICE"] . '</Price>
                                <VAT>18</VAT>
                                <VatSum>' . $nds_sum . '</VatSum>
                                <TotalWithoutVat>' . $no_vat . '</TotalWithoutVat>
                                <TotalWithVat>' . $item["FULL_PRICE"] . '</TotalWithVat>

                            </Item>';
            }

            $out .= '</Items>
                </Order>
            </Orders>
        </Command>';

            $url = "https://3pl.spsr.ru/webapi/execute";

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
            curl_setopt($ch, CURLOPT_TIMEOUT, 500);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $out);

            $req = curl_exec($ch);
            curl_close($ch);

            /* echo "<pre>";
              var_dump( $out );
              echo "</pre>";
              die; */


            mail("delllevit@gmail.com", "Отправка заказа", print_r($req, true));
            return true;
        }
        return false;
    }

}
