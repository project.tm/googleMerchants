<?php

namespace Local\Helper\Feed;

use CModule,
    CCatalogSKU;

class GoogleMerchants {

    const FEED_PATH = '/local/feed/google.merchants.feed.txt';
    const LIMIT = 5000;
    const UF_GOOGLE_CATEGORY = 'UF_GOOGLE_CATEGORY';

    static public function agent() {
//        global $USER;
//        if(empty($USER)) {
//            $GLOBALS["USER"] = new \CUser;
//        }
//        if(!isDebugTest()) {
//            return '\Local\Helper\Feed\GoogleMerchants::agent('.date('i,s').');';
//        }
        try {
//            \Local\Utility::useCache(array(__CLASS__, __FUNCTION__), function() {
//                return true;
//            }, 1800);
//            if (\Local\Utility::isCache() and 0) {
//                $content = file_get_contents($_SERVER["DOCUMENT_ROOT"] . self::FEED_PATH);
//            } else {
//                ob_start();
                self::view();
//                file_put_contents($_SERVER["DOCUMENT_ROOT"] . self::FEED_PATH, $content = ob_get_clean());
//            }
//            echo $content;
        } catch (Exception $ex) {
            
        }
        return '\Local\Helper\Feed\GoogleMerchants::agent();';
    }

    static public function view() {
        if (CModule::IncludeModule('iblock') and CModule::IncludeModule('catalog')) {
            global $arFilter;
            $arFilter = array(
                '!PROPERTY_SHOW' => false,
                '!PREVIEW_PICTURE' => false,
//                'ID' => 94958,
            );
            if(isDebug()) {
//                $arFilter['ID'] = 94958;
            }

            $arParams["IBLOCK_ID"] = \Local\Helper\Sections::IBLOCK_ID;
            $arParams = array(
                'IBLOCK_TYPE' => '1c_catalog',
                'IBLOCK_ID2' => $arParams["IBLOCK_ID"],
                'IBLOCK_ID' => CCatalogSKU::GetInfoByProductIBlock($arParams["IBLOCK_ID"])["IBLOCK_ID"],
                'ELEMENT_SORT_FIELD' => 'property_SORT_LOOK',
                'ELEMENT_SORT_ORDER' => 'asc',
                'ELEMENT_SORT_FIELD2' => 'shows',
                'ELEMENT_SORT_ORDER2' => 'asc',
                'PROPERTY_CODE_VIEW' => 'Y',
                'PROPERTY_CODE' =>
                array(
                    'RAZMER',
                    'ARTIKUL_POSTAVSHCHIKA',
                    'TSVET',
                    'NEWPRODUCT',
                    'SALELEADER',
                    'SPECIALOFFER',
                    'BREND',
                    'CML2_LINK.DETAIL_TEXT'
                ),
                'PROPERTY_CODE_ADD' =>
                array(
                    0 => "CML2_ARTICLE",
                    1 => "MORE_PHOTO",
                    2 => "POSTAVSHCHIK",
                    3 => "RAZMER",
                    4 => "ARTIKUL",
                    5 => "ARTIKUL_POSTAVSHCHIKA",
                    6 => "TSVET",
                ),
                'META_KEYWORDS' => '-',
                'META_DESCRIPTION' => '-',
                'BROWSER_TITLE' => '-',
                'SET_LAST_MODIFIED' => false,
                'INCLUDE_SUBSECTIONS' => 'Y',
                'BASKET_URL' => '/personal/cart/',
                'ACTION_VARIABLE' => 'action',
                'PRODUCT_ID_VARIABLE' => 'id',
                'SECTION_ID_VARIABLE' => 'SECTION_ID',
                'PRODUCT_QUANTITY_VARIABLE' => 'quantity',
                'PRODUCT_PROPS_VARIABLE' => 'prop',
                'FILTER_NAME' => 'arFilter',
                'CACHE_TYPE' => 'N',
                'CACHE_TIME' => '36000000',
                'CACHE_FILTER' => true,
                'CACHE_GROUPS' => 'Y',
                'SET_TITLE' => true,
                'MESSAGE_404' => '',
                'SET_STATUS_404' => 'Y',
                'SHOW_404' => 'N',
                'FILE_404' => NULL,
                'DISPLAY_COMPARE' => false,
                'PAGE_ELEMENT_COUNT' => self::LIMIT,
                'LINE_ELEMENT_COUNT' => 3,
                'PRICE_CODE' =>
                array(
                    0 => 'Розничная',
                ),
                'USE_PRICE_COUNT' => false,
                'SHOW_PRICE_COUNT' => 1,
                'PRICE_VAT_INCLUDE' => true,
                'USE_PRODUCT_QUANTITY' => true,
                'ADD_PROPERTIES_TO_BASKET' => 'N',
                'PARTIAL_PRODUCT_PROPERTIES' => 'N',
                'PRODUCT_PROPERTIES' =>
                array(
                ),
                'DISPLAY_TOP_PAGER' => false,
                'DISPLAY_BOTTOM_PAGER' => true,
                'PAGER_TITLE' => 'Товары',
                'PAGER_SHOW_ALWAYS' => false,
                'PAGER_TEMPLATE' => 'modern',
                'PAGER_DESC_NUMBERING' => false,
                'PAGER_DESC_NUMBERING_CACHE_TIME' => 36000000,
                'PAGER_SHOW_ALL' => false,
                'PAGER_BASE_LINK_ENABLE' => 'N',
                'PAGER_BASE_LINK' => NULL,
                'PAGER_PARAMS_NAME' => NULL,
                'OFFERS_CART_PROPERTIES' =>
                array(
                ),
                'OFFERS_FIELD_CODE' =>
                array(
                    0 => 'DETAIL_PICTURE',
                    1 => 'DETAIL_TEXT',
                ),
                'OFFERS_PROPERTY_CODE' =>
                array(
                    1 => 'TSVET_2',
                ),
                'OFFERS_SORT_FIELD' => 'shows',
                'OFFERS_SORT_ORDER' => 'asc',
                'OFFERS_SORT_FIELD2' => 'shows',
                'OFFERS_SORT_ORDER2' => 'asc',
                'OFFERS_LIMIT' => 0,
                'SECTION_ID' => 0,
                'SECTION_CODE' => '',
                'SECTION_URL' => '/catalog/#SECTION_CODE_PATH#/',
                'DETAIL_URL' => '/catalog/#SECTION_CODE_PATH#/#ELEMENT_CODE#/',
                'USE_MAIN_ELEMENT_SECTION' => false,
                'CONVERT_CURRENCY' => 'N',
                'CURRENCY_ID' => '',
                'HIDE_NOT_AVAILABLE' => 'N',
                'LABEL_PROP' => '-',
                'ADD_PICT_PROP' => '-',
                'PRODUCT_DISPLAY_MODE' => 'N',
                'OFFER_ADD_PICT_PROP' => '-',
                'OFFER_TREE_PROPS' =>
                array(
                ),
                'PRODUCT_SUBSCRIPTION' => NULL,
                'SHOW_DISCOUNT_PERCENT' => 'Y',
                'SHOW_OLD_PRICE' => 'Y',
                'MESS_BTN_BUY' => 'Купить',
                'MESS_BTN_ADD_TO_BASKET' => 'В корзину',
                'MESS_BTN_SUBSCRIBE' => NULL,
                'MESS_BTN_DETAIL' => 'Подробнее',
                'MESS_NOT_AVAILABLE' => 'Нет в наличии',
                'TEMPLATE_THEME' => 'site',
                'ADD_SECTIONS_CHAIN' => false,
                'ADD_TO_BASKET_ACTION' => NULL,
                'SHOW_CLOSE_POPUP' => 'N',
                'COMPARE_PATH' => '/catalog/compare/?action=COMPARE',
                'SHOW_ALL_WO_SECTION' => 'Y',
            );
            global $APPLICATION;
            $APPLICATION->IncludeComponent("classylook:catalog.section", "google.merchants.feed", $arParams);
        }
    }

}
