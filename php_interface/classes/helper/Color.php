<?php

namespace Local\Helper;

use Bitrix\Highloadblock as HL,
    CDBResult,
    Local\Utility;

class Color {

    static public function getByCode($code) {
        static $arColors = array();
        if (empty($arColors)) {
            $arColors = Utility::useCache(array(__CLASS__, __FUNCTION__), function() {
                        $arColors = array();
                        $hlblock = HL\HighloadBlockTable::getById(1)->fetch();
                        $entity = HL\HighloadBlockTable::compileEntity($hlblock);
                        $entity_data_class = $entity->getDataClass();
                        $entity_table_name = $hlblock['TABLE_NAME'];

                        $sTableID = 'tbl_' . $entity_table_name;
                        $rsData = $entity_data_class::getList(array(
                                    "select" => array('UF_CODE', 'UF_NAME')
                        ));
                        $rsData = new CDBResult($rsData, $sTableID);
                        while ($arRes = $rsData->Fetch()) {
                            $arColors[$arRes['UF_CODE']] = $arRes['UF_NAME'];
                        }
                        return $arColors;
                    });
        }
        return isset($arColors[$code]) ? $arColors[$code] : '';
    }

}
