<?php

namespace Local\Helper;

use CIBlockSection;

class Sections {

    const IBLOCK_ID = 46;

    public static function getSection($ID) {
        static $caregoryList = array();
        if (empty($caregoryList)) {

            $categoryClass = \Local\Highload::get('GoogleCategory');
            $rsData = $categoryClass::getList(array(
                        "select" => array('ID', 'UF_CODE', 'UF_GOOGLE'),
            ));
            $arCatehory = array();
            $rsData = new \CDBResult($rsData);
            while ($arItem = $rsData->Fetch()) {
                $arCatehory[$arItem['ID']] = $arItem['UF_CODE'];
            }

            $arFilter = Array('IBLOCK_ID' => self::IBLOCK_ID);
            $db_list = CIBlockSection::GetList(Array('DEPTH_LEVEL' => 'ASC'), $arFilter, false, array('ID', 'IBLOCK_SECTION_ID', 'UF_GOOGLE_CATEGORY'));
            while ($arSection = $db_list->Fetch()) {
                if (empty($arSection['UF_GOOGLE_CATEGORY'])) {
                    $caregoryList[$arSection['ID']] = $caregoryList[$arSection['IBLOCK_SECTION_ID']];
                } else {
                    $caregoryList[$arSection['ID']] = $arCatehory[$arSection['UF_GOOGLE_CATEGORY']];
                }
            }
        }
        return isset($caregoryList[$ID]) ? $caregoryList[$ID] : '';
    }

}
