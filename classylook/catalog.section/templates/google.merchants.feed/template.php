<?

use Bitrix\Main\Context,
    Bitrix\Main\Loader,
    Bitrix\Main\Type\Collection,
    Bitrix\Main\Type\DateTime,
    Bitrix\Main,
    Bitrix\Currency,
    Bitrix\Catalog,
    Bitrix\Iblock;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
CModule::IncludeModule("highloadblock");

$filter = function($string, $limit) {
    return '"' . TruncateText(str_replace('"', "'", str_replace(array(PHP_EOL, "\t"), ' ', strip_tags($string))), $limit - 3) . '"';
};

$arItem = array(
    'id',
    'title',
    'description',
    'link',
    'image link',
    'availability ',
    'price',
    'google product category',
    'brand',
    'mpn',
    'condition',
    'adult',
    'color',
    'gender',
    'size',
);
//$arItem = array(
//    'идентификатор',
//    'название',
//    'описание',
//    'ссылка',
//    'ссылка_​на_​изображение',
//    'наличие',
//    'цена',
//    'категория_​товара_​в_​google',
//    'марка',
//    'mpn',
//    'состояние',
//    'взрослые',
//    'цвет',
//    'пол',
//    'размер',
//);

echo implode("\t", $arItem);
$idList = array();
foreach ($arResult['ITEMS'] as $item) {
    if (!empty($idList[$item['ID']])) {
        continue;
    }
    $idList[$item['ID']] = 1;
    echo PHP_EOL;
    $offers = false;
    $colors = $sizes = $colors2 = array();
    foreach ($item['OFFERS'] as $key => $value) {
        if (empty($offers)) {
            $offers = $value;
        }
        $key = \Local\Helper\Color::getByCode($key);
        if (count($colors) < 3) {
            $colors[$key] = str_replace('-', ' ', $key);
        }
        $colors2[$key] = str_replace('-', ' ', $key);
        if ($value['RAZMER']) {
            $sizes[$value['RAZMER']] = $value['RAZMER'];
        }
    }

    if (!$googleCategory = \Local\Helper\Sections::getSection($item['IBLOCK_SECTION_ID'])) {
        $db_old_groups = CIBlockElement::GetElementGroups($item['ID'], true, array('ID'));
        while ($ar_group = $db_old_groups->Fetch()) {
            if ($googleCategory = \Local\Helper\Sections::getSection($ar_group['ID'])) {
                break;
            }
        }
    }

    if (empty($item['CML2_LINK_DETAIL_TEXT'])) {
        $rsElement = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $arParams["IBLOCK_ID2"], 'ID' => $item['ID']), false, false, array('*'));
        if ($obElement = $rsElement->GetNextElement()) {
            $arResult2 = $obElement->GetFields();

            $arResult2["PROPERTIES"] = $obElement->GetProperties();
            $arResult2["DISPLAY_PROPERTIES"] = array();
            $propertyList = array();
            if (!empty($arParams['PROPERTY_CODE_ADD'])) {
                $selectProperties = array_fill_keys($arParams['PROPERTY_CODE_ADD'], true);
                $propertyIterator = Iblock\PropertyTable::getList(array(
                            'select' => array('ID', 'CODE'),
                            'filter' => array('=IBLOCK_ID' => $arParams['IBLOCK_ID2'], '=ACTIVE' => 'Y'),
                            'order' => array('SORT' => 'ASC', 'ID' => 'ASC')
                ));
                while ($property = $propertyIterator->fetch()) {
                    $code = (string) $property['CODE'];
                    if ($code == '')
                        $code = $property['ID'];
                    if (!isset($selectProperties[$code]))
                        continue;
                    $propertyList[] = $code;
                    unset($code);
                }
                unset($property, $propertyIterator);
                unset($selectProperties);
            }
            if (!empty($propertyList)) {
                foreach ($propertyList as &$pid) {
                    if (!isset($arResult2["PROPERTIES"][$pid]))
                        continue;
                    $prop = &$arResult2["PROPERTIES"][$pid];
                    $boolArr = is_array($prop["VALUE"]);
                    if (
                            ($boolArr && !empty($prop["VALUE"])) || (!$boolArr && strlen($prop["VALUE"]) > 0)
                    ) {
                        $arResult2["DISPLAY_PROPERTIES"][$pid] = CIBlockFormatProperties::GetDisplayValue($arResult2, $prop, "catalog_out");
                    }
                    unset($prop);
                }
                unset($pid);
            }
            $item['CML2_LINK_DETAIL_TEXT'] = array();
            foreach ($arResult2["DISPLAY_PROPERTIES"] as $prop) {
                $item['CML2_LINK_DETAIL_TEXT'][] = $prop["NAME"] . ': ' . $prop["VALUE"];
            }
            $item['CML2_LINK_DETAIL_TEXT'][] = 'Цвет: ' . implode(', ', $colors2);
            $item['CML2_LINK_DETAIL_TEXT'] = implode('; ', $item['CML2_LINK_DETAIL_TEXT']);
            if(empty($item['CML2_LINK_DETAIL_TEXT'])) {
                $item['CML2_LINK_DETAIL_TEXT'] = $item['NAME'] . '; ' . implode(', ', $colors2);
            }
        }
    }

    $item['CML2_LINK_DETAIL_TEXT'] = strip_tags($item['CML2_LINK_DETAIL_TEXT']);
    $arItem = array(
        $item['ID'],
        $filter($item['NAME'], 150),
//        $filter(empty($item['CML2_LINK_DETAIL_TEXT']) ? $item['NAME'] .'; '. implode(', ', $colors2) : $item['CML2_LINK_DETAIL_TEXT'], 5000),
        $filter($item['CML2_LINK_DETAIL_TEXT'], 5000),
        'http://' . SITE_SERVER_NAME . $item['DETAIL_PAGE_URL'],
        'http://' . SITE_SERVER_NAME . $offers['PREVIEW_PICTURE']['SRC'],
        'в наличии',
        $item['PRICE'] . ' RUB',
        $filter($googleCategory, 5000),
        $item['BREND'],
        $item['ARTIKUL_POSTAVSHCHIKA'],
        'новый',
        'нет',
        implode('/', $colors),
        'женский',
        implode('/', $sizes),
    );
//    pre($arItem, $item);
    echo implode("\t", $arItem);
}
